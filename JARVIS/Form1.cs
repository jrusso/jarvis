﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.AudioFormat;
using System.Windows.Forms;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using CloudSpeech;
using JARVIS;
using JARVIS.JarvisEngines;
using RestSharp.Extensions;

namespace JARVISV2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Form1 : Form
    {
        private readonly SpeechRecognitionEngine _recognizer;

        public static bool IsSleeping = false;

        SpeechSynthesizer JARVIS = new SpeechSynthesizer();
        String Temperature;
        String Condition;
        String Humidity;
        String WinSpeed;
        String TFCond;
        String TFHigh;
        String TFLow;
        String WOEID = "2405528"; //<<<<-------- CHANGE THE WOEID CODE HERE
        String Town;
        String QEvent;
        DateTime now = DateTime.Now;
        String userName = Environment.UserName;
        Random rnd = new Random();
        int timer = 11;
        int count = 1;

        private static bool ShouldRemoveWaiting = false;

        private static string TextToRemove = string.Empty;

        public Form1()
        {
            InitializeComponent();

            _recognizer = new DefaultJarvisRecognitionEngine(JARVIS);

         //   _recognizer.SetInputToDefaultAudioDevice();
         ////   CleanUpXml();
         //   //_recognizer.LoadCleanUpXmlGrammar(new Grammar(new GrammarBuilder(new Choices(File.ReadAllLines(@"Commands.txt")))));
          
         //   _recognizer.LoadGrammarAsync(new DictationGrammar("grammar:dictation"));
           
         //   _recognizer.LoadGrammarCompleted += RecognizerOnLoadGrammarCompleted;
            
         //   _recognizer.SpeechRecognized += _recognizer_SpeechRecognized;
         //   _recognizer.RecognizeAsync(RecognizeMode.Multiple);

            //  GenerateXmlFromTxt();
        }

        private void RecognizerOnLoadGrammarCompleted(object sender, LoadGrammarCompletedEventArgs loadGrammarCompletedEventArgs)
        {
            JARVIS.Speak("I am now online sir");
        }

        private void CleanUpXml()
        {
            var document = XDocument.Load(@"../../GrammarSets.xml");
            var set = document.Descendants("GrammarSet");



            var nodeSet = set.FirstOrDefault(a => a.Attribute("Type").Value == "Words");

            var grammars = nodeSet.Descendants().ToList();


            foreach (var element in grammars)
            {
                try
                {

                    var currentText = element.Attribute("Text").Value;
                    if (currentText.Length <= 3)
                    {
                        element.Remove();
                    }
                    if (currentText.Contains(" "))
                    {
                        element.Remove();
                    }
                    if (currentText.Contains("-"))
                    {
                        element.Remove();
                    }
                    if (currentText.IsUpperCase())
                    {
                        element.Remove();
                    }
                }
                catch (Exception)
                {
                    continue;

                }
            }



            document.Save(@"../../GrammarSets.xml");
        }

        private void GenerateXmlFromTxt()
        {
            var stream = new StreamReader(@"../../part-of-speech.txt");

            string line;

            var document = XDocument.Load(@"../../GrammarSets.xml");

            var wordSet = document.Descendants("GrammarSet").FirstOrDefault(a => a.Attribute("Type").Value == "Words");

            if (wordSet == null)
                throw new Exception("Invalid format");




            while ((line = stream.ReadLine()) != null)
            {
                var temp = line.Split('\t');

                var word = temp[0];

                var element = new XElement("Grammar");

                element.SetAttributeValue("Text", word);

                wordSet.Add(element);
            }

            document.Save("../../GrammarSets.xml", SaveOptions.None);
        }

    
        
        void _recognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            string time = now.GetDateTimeFormats('t')[0];
          
            int ranNum;

            string speech = e.Result.Text;

          DoWithResponseFromGoogle(e, speech);
           
            //switch (speech.ToLower())
            //{

            //    //GREETINGS
            //    case "hello":
            //    case "hello Jarvis":
            //        if (now.Hour >= 5 && now.Hour < 12)
            //        { JARVIS.Speak("Goodmorning " + userName); }
            //        if (now.Hour >= 12 && now.Hour < 18)
            //        { JARVIS.Speak("Good afternoon " + userName); }
            //        if (now.Hour >= 18 && now.Hour < 24)
            //        { JARVIS.Speak("Good evening " + userName); }
            //        if (now.Hour < 5)
            //        { JARVIS.Speak("Hello, it is getting late " + userName); }
            //        break;
            //    case "goodbye":
            //    case "goodbye Jarvis":
            //    case "close":
            //    case "close Jarvis":
            //        JARVIS.Speak("Farewell");
            //        Close();
            //        break;
            //    case "jarvis":
            //        ranNum = rnd.Next(1, 3);
            //        if (ranNum == 1) { QEvent = ""; JARVIS.Speak("Yes sir"); }
            //        else if (ranNum == 2) { QEvent = ""; JARVIS.Speak("Yes?"); }
            //        break;

            //    //CONDITION OF DAY
            //    case "what time is it":
            //        JARVIS.Speak(time);
            //        break;
            //    case "what day is it":
            //        JARVIS.Speak(DateTime.Today.ToString("dddd"));
            //        break;
            //    case "Whats the date":
            //    case "Whats todays date":
            //        JARVIS.Speak(DateTime.Today.ToString("dd-MM-yyyy"));
            //        break;
            //    case "hows the weather":
            //    case "whats the weather like":
            //    case "whats it like outside":
            //        GetWeather();
            //        if (QEvent == "connected")
            //        { JARVIS.Speak("The weather in " + Town + " is " + Condition + " at " + Temperature + " degrees. There is a humidity of " + Humidity + " and a windspeed of " + WinSpeed + " miles per hour"); }
            //        else if (QEvent == "failed")
            //        { JARVIS.Speak("I seem to be having a bit of trouble connecting to the server. Just look out the window"); }
            //        break;
            //    case "what will tomorrow be like":
            //    case "whats tomorrows forecast":
            //    case "whats tomorrow like":
            //        GetWeather();
            //        if (QEvent == "connected")
            //        { JARVIS.Speak("Tomorrows forecast is " + TFCond + " with a high of " + TFHigh + " and a low of " + TFLow); }
            //        else if (QEvent == "failed")
            //        { JARVIS.Speak("It's hard to tell without an stable internet connection"); }
            //        break;
            //    case "Whats the temperature outside":
            //        GetWeather();
            //        if (QEvent == "connected")
            //        { JARVIS.Speak(Temperature + " degrees"); }
            //        else if (QEvent == "failed")
            //        { JARVIS.Speak("I cannot access the server at this time"); }
            //        break;

            //    //OTHER COMMANDS
            //    case "Switch Window":
            //        SendKeys.Send("%{TAB " + count + "}");
            //        count += 1;
            //        break;
            //    case "Reset":
            //        count = 1;
            //        int timer = 11;
            //        lblTimer.Visible = false;
            //        ShutdownTimer.Enabled = false;
            //        lstCommands.Visible = false;
            //        break;
            //    case "Out of the way":
            //        if (WindowState == FormWindowState.Normal)
            //        {
            //            WindowState = FormWindowState.Minimized;
            //            JARVIS.Speak("My apologies");
            //        }
            //        break;
            //    case "Come back":
            //        if (WindowState == FormWindowState.Minimized)
            //        {
            //            JARVIS.Speak("Alright?");
            //            WindowState = FormWindowState.Normal;
            //        }
            //        break;
            //    case "Show commands":
            //        string[] commands = File.ReadAllLines("Commands.txt");
            //        JARVIS.Speak("Here we are");
            //        lstCommands.Items.Clear();
            //        lstCommands.SelectionMode = SelectionMode.None;
            //        lstCommands.Visible = true;
            //        foreach (string command in commands)
            //        {
            //            lstCommands.Items.Add(command);
            //        }
            //        break;
            //    case "Hide listbox":
            //        lstCommands.Visible = false;
            //        break;

            //    //SHUTDOWN RESTART LOG OFF
            //    case "Shutdown":
            //        if (ShutdownTimer.Enabled == false)
            //        {
            //            QEvent = "shutdown";
            //            JARVIS.Speak("I will shutdown shortly");
            //            lblTimer.Visible = true;
            //            ShutdownTimer.Enabled = true;
            //        }
            //        break;
            //    case "Log off":
            //        if (ShutdownTimer.Enabled == false)
            //        {
            //            QEvent = "logoff";
            //            JARVIS.Speak("Logging off");
            //            lblTimer.Visible = true;
            //            ShutdownTimer.Enabled = true;
            //        }
            //        break;
            //    case "Restart":
            //        if (ShutdownTimer.Enabled == false)
            //        {
            //            QEvent = "restart";
            //            JARVIS.Speak("I'll be back shortly");
            //            lblTimer.Visible = true;
            //            ShutdownTimer.Enabled = true;
            //        }
            //        break;
            //    case "Abort":
            //        if (ShutdownTimer.Enabled == true)
            //        {
            //            timer = 11;
            //            lblTimer.Text = timer.ToString();
            //            ShutdownTimer.Enabled = false;
            //            lblTimer.Visible = false;
            //        }
            //        break;
            //    default:
            //        DecisionToRemove(speech);
            //        break;
            //}
        }

        private void DoWithResponseFromGoogle(SpeechRecognizedEventArgs e, string speech)
        {
            speech = JarvisEventFactory.CheckAgainstGoogle(e);

            JarvisEventFactory.DoBrainWork(speech, JARVIS, _recognizer, _recognizer_SpeechRecognized);
        }

       

        private void RemoveFromXmlDoc()
        {
            var doc = XDocument.Load(@"../../GrammarSets.xml");

            var element = doc.Descendants("Grammar").FirstOrDefault(a => a.Attribute("Text").Value == TextToRemove);
         
            if(element != null)
                element.Remove();

            doc.Save(@"../../GrammarSets.xml");

            JARVIS.Speak("Let me rebuild the database for you sir.");

            _recognizer.UnloadAllGrammars();

            //_recognizer.LoadGrammar(InitGrammarSet());

        }

        private void DecisionToRemove(string speech)
        {

            JARVIS.Speak("You said: " + speech + ". Is this correct?");
            TextToRemove = speech;
            ShouldRemoveWaiting = true;

        }

        private void GetWeather()
        {
            string query = String.Format("http://weather.yahooapis.com/forecastrss?w=" + WOEID);
            XmlDocument wData = new XmlDocument();
            wData.Load(query);

            XmlNamespaceManager man = new XmlNamespaceManager(wData.NameTable);
            man.AddNamespace("yweather", "http://xml.weather.yahoo.com/ns/rss/1.0");

            XmlNode channel = wData.SelectSingleNode("rss").SelectSingleNode("channel");
            XmlNodeList nodes = wData.SelectNodes("/rss/channel/item/yweather:forecast", man);

            Temperature = channel.SelectSingleNode("item").SelectSingleNode("yweather:condition", man).Attributes["temp"].Value;

            Condition = channel.SelectSingleNode("item").SelectSingleNode("yweather:condition", man).Attributes["text"].Value;

            Humidity = channel.SelectSingleNode("yweather:atmosphere", man).Attributes["humidity"].Value;

            WinSpeed = channel.SelectSingleNode("yweather:wind", man).Attributes["speed"].Value;

            Town = channel.SelectSingleNode("yweather:location", man).Attributes["city"].Value;

            TFCond = channel.SelectSingleNode("item").SelectSingleNode("yweather:forecast", man).Attributes["text"].Value;

            TFHigh = channel.SelectSingleNode("item").SelectSingleNode("yweather:forecast", man).Attributes["high"].Value;

            TFLow = channel.SelectSingleNode("item").SelectSingleNode("yweather:forecast", man).Attributes["low"].Value;

            QEvent = "connected";
        }

        private void ShutdownTimer_Tick(object sender, EventArgs e)
        {
            if (timer == 0)
            {
                lblTimer.Visible = false;
                ComputerTermination();
                ShutdownTimer.Enabled = false;
            }
            else
            {
                timer = timer - 1;
                lblTimer.Text = timer.ToString();
            }
        }
        private void ComputerTermination()
        {
            switch (QEvent)
            {
                case "shutdown":
                    System.Diagnostics.Process.Start("shutdown", "-s");
                    break;
                case "logoff":
                    System.Diagnostics.Process.Start("shutdown", "-l");
                    break;
                case "restart":
                    System.Diagnostics.Process.Start("shutdown", "-r");
                    break;
            }
        }
    }
}