﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace JARVIS.Services
{
    public class WSSQL
    {
        const string connectionString = "Provider=Search.CollatorDSO;Extended Properties=\"Application=Windows\"";

        static List<string> AvailableOptions = new List<string>();

        // Display the result set recursively expanding chapterDepth deep
        static void DisplayReader(OleDbDataReader myDataReader, ref uint count, uint alignment, int chapterDepth)
        {
            try
            {
                // compute alignment
                StringBuilder indent = new StringBuilder((int)alignment);
                indent.Append(' ', (int)alignment);

                while (myDataReader.Read())
                {
                    // add alignment
                    StringBuilder row = new StringBuilder(indent.ToString());

                    // for all columns
                    for (int i = 0; i < myDataReader.FieldCount; i++)
                    {
                        // null columns
                        if (myDataReader.IsDBNull(i))
                        {
                            row.Append("NULL;");
                        }
                        else
                        {
                            //vector columns
                            object[] myArray = myDataReader.GetValue(i) as object[];
                            if (myArray != null)
                            {
                                bool first = true;
                                row.Append('[');
                                foreach (object myObj in myArray)
                                {
                                    if (first)
                                    {
                                        first = false;
                                    }
                                    else
                                    {
                                        row.Append(", ");
                                    }
                                    row.Append(myObj);
                                }
                                row.Append(']');
                            }
                            else
                            {
                                //check for chapter columns from "group on" queries
                                if (myDataReader.GetFieldType(i).ToString() != "System.Data.IDataReader")
                                {
                                    //regular columns are displayed here
                                    row.Append(myDataReader.GetValue(i));
                                }
                                else
                                {
                                    //for a chapter column type just display the colum name
                                    row.Append(myDataReader.GetName(i));
                                }
                            }
                            row.Append(';');
                        }
                    }
                    if (chapterDepth >= 0)
                    {

                        AvailableOptions.Add(row.ToString());

                        count++;
                    }
                    // for each chapter column
                    for (int i = 0; i < myDataReader.FieldCount; i++)
                    {
                        if (myDataReader.GetFieldType(i).ToString() == "System.Data.IDataReader")
                        {
                            OleDbDataReader Reader = myDataReader.GetValue(i) as OleDbDataReader;
                            DisplayReader(Reader, ref count, alignment + 8, chapterDepth - 1);
                        }
                    }
                }
            }
            finally
            {
                myDataReader.Close();
                myDataReader.Dispose();
            }
        }

        public static List<string> GetApplication(string name)
        {
            string query = "SELECT System.ItemUrl,  System.ItemName, System.ItemDate, System.Kind FROM SystemIndex WHERE System.ItemName like '%" + name + "%'";

            AvailableOptions.Clear();

            ExecuteQuery(query, 100000000);

            return AvailableOptions;
        }

        // Execute a query and display the rowset up to chapterDepth deep
        static void ExecuteQuery(string query, int chapterDepth)
        {
            OleDbDataReader myDataReader = null;
            OleDbConnection myOleDbConnection = new OleDbConnection(connectionString);
            OleDbCommand myOleDbCommand = new OleDbCommand(query, myOleDbConnection);
            try
            {
                Console.WriteLine("Query=" + query);
                myOleDbConnection.Open();
                myDataReader = myOleDbCommand.ExecuteReader();
                if (!myDataReader.HasRows)
                {
                    System.Console.WriteLine("Query returned 0 rows!");
                    return;
                }
                uint count = 0;
                DisplayReader(myDataReader, ref count, 0, chapterDepth);
                Console.WriteLine("Rows+Chapters=" + count);
            }
            catch (System.Data.OleDb.OleDbException oleDbException)
            {
                Console.WriteLine("Got OleDbException, error code is 0x{0:X}L", oleDbException.ErrorCode);
                Console.WriteLine("Exception details:");
                for (int i = 0; i < oleDbException.Errors.Count; i++)
                {
                    Console.WriteLine("\tError " + i + "\n" +
                                      "\t\tMessage: " + oleDbException.Errors[i].Message + "\n" +
                                      "\t\tNative: " + oleDbException.Errors[i].NativeError.ToString() + "\n" +
                                      "\t\tSource: " + oleDbException.Errors[i].Source + "\n" +
                                      "\t\tSQL: " + oleDbException.Errors[i].SQLState + "\n");
                }
                Console.WriteLine(oleDbException.ToString());
            }
            finally
            {
                // Always call Close when done reading.
                if (myDataReader != null)
                {
                    myDataReader.Close();
                    myDataReader.Dispose();
                }
                // Close the connection when done with it.
                if (myOleDbConnection.State == System.Data.ConnectionState.Open)
                {
                    myOleDbConnection.Close();
                }
            }
        }

    }
}
