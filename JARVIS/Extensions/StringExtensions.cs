﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JARVIS.Extensions
{
    public static class StringExtensions
    {
        public static string Clean(this string value )
        {
            var temp = value.ToLower();

            value = value.ToLower();

            if (value.Contains(" "))
            {
                temp = temp.Replace(" ", "");
            }
            if (value.Contains("-"))
            {
                temp = temp.Replace("-", "");
            }

            temp = temp.Trim();

            return temp;
        }
    }
}
