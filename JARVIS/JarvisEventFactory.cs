﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Text;
using System.Xml.Linq;
using CloudSpeech;
using CSCore;
using JARVIS.Properties;

namespace JARVIS
{
    public static class JarvisEventFactory
    {
        private static SpeechRecognitionEngine Engine;
        private static EventHandler<SpeechRecognizedEventArgs> OriginalEvent;
        static int JarvisCount = 0;
        private static SpeechSynthesizer JARVIS;

        public static string CheckAgainstGoogle(RecognitionEventArgs e)
        {
            string speech = string.Empty;
            var stream = new MemoryStream();

            e.Result.Audio.WriteToWaveStream(stream);

            var stt = new SpeechToText();

            using (var stream2 = new MemoryStream(stream.ToArray()))
            {
                var response = stt.Recognize(stream2);

                if (response.Any())
                {
                    speech = response.FirstOrDefault().Utterance;
                }
            }
            return speech;
        }

        public static void DoBrainWork(string text, SpeechSynthesizer mouth,
            SpeechRecognitionEngine engine,
            EventHandler<SpeechRecognizedEventArgs> recognizerSpeechRecognized)
        {
            InitVariables(mouth, engine, recognizerSpeechRecognized);

            text = text.ToLower();

            CheckAgainstJarvis(text);

            var elementExists  = InputExistsInXml(text);

            if (!Settings.Default.IsWaitingForResponse && elementExists != null)
            {
                if (elementExists.Attribute("Type").Value == "Command")
                {
                    if (elementExists.Attribute("Url") != null)
                    {
                        
                    }
                    else
                    {
                        mouth.SpeakAsync("Would you like me to search for that on disk?");

                        engine.SpeechRecognized -= recognizerSpeechRecognized;

                        engine.SpeechRecognized += (obj, e) => CommandEventHandler(obj, e, elementExists);
                    }
                
                }
                else if(elementExists.Attribute("Type").Value == "Word")
                {
                    if (elementExists.Attribute("Response") != null)
                    {
                        var response = elementExists.Attribute("Response").Value;
                        
                        mouth.Speak(response);
                    }
                    mouth.Speak(elementExists.Attribute("Text").Value);
                }
            }
            else
            {
                if (Settings.Default.IsWaitingForResponse)
                {
                    var textToAddToXml = Settings.Default.IsWaitingForResponseForWord;

                    if (text == "no")
                    {
                        // not a command
                        AddToDatabase(textToAddToXml, "Word");
                    }
                    else if(text == "yes")
                    {
                        //command
                        AddToDatabase(textToAddToXml, "Command");
                    }
                }
                else
                {
                    mouth.Speak("I do not understand sir, is this a command?");

                    Settings.Default.IsWaitingForResponse = true;

                    Settings.Default.IsWaitingForResponseForWord = text;
                    
                    Settings.Default.Save();
                }
            }
        }

        private static void InitVariables(SpeechSynthesizer mouth, SpeechRecognitionEngine engine,
            EventHandler<SpeechRecognizedEventArgs> recognizerSpeechRecognized)
        {
            if (Engine == null)
            {
                Engine = engine;
            }
            if (OriginalEvent == null)
            {
                OriginalEvent = recognizerSpeechRecognized;
            }
            if (JARVIS == null)
            {
                JARVIS = mouth;
            }
        }

        private static void CommandEventHandler(object sender, SpeechRecognizedEventArgs e, XElement element)
        {
            var option = CheckAgainstGoogle(e);

            if (option.ToLower() == "yes")
            {
                var startingPath = @"C:\Program Files";

                var files = Directory.GetFiles(startingPath, "*"+ element.Attribute("Text").Value + "*" , SearchOption.AllDirectories);

                if (!files.Any())
                {
                    startingPath = @"I:\SteamLibrary\";

                    var file = SearchDirectory(startingPath, element.Attribute("Text").Value);

                    if (file != string.Empty)
                    {
                        
                    }
                }
            }
            else
            {
                JARVIS.Speak("Ok");
            }

            Engine.SpeechRecognized -= (obj, ev) => CommandEventHandler(obj, ev, element);

            Engine.SpeechRecognized += OriginalEvent;
        }

        private static string SearchDirectory(string startingPath, string fileName)
        {
            var directories = Directory.GetDirectories(startingPath);

            foreach (var directory in directories)
            {
                var files = Directory.GetFiles(directory);

                if (
                    !files.Any(
                        a =>
                            Path.GetFileName(a).ToLower().Trim().Replace(" ", "") ==
                            fileName.ToLower().Trim().Replace(" ", "")))
                {
                    SearchDirectory(directory, fileName);
                }
                else
                {
                    return files.FirstOrDefault( a =>
                            Path.GetFileName(a).ToLower().Trim().Replace(" ", "") ==
                            fileName.ToLower().Trim().Replace(" ", ""));
                }
            }
            return string.Empty;
        }

        private static void CheckAgainstJarvis(string text)
        {
            if (text == "jarvis")
            {
                JarvisCount++;
            }

            if (JarvisCount >= 2)
            {
                Settings.Default.IsWaitingForResponse = false;
                Settings.Default.IsWaitingForResponseForWord = string.Empty;
                Settings.Default.Save();
                JarvisCount = 0;
            }
        }



        private static void AddToDatabase(string textToAddToXml, string command)
        {
            var doc = XDocument.Load(Properties.Settings.Default.BrainXmlLocation + Properties.Settings.Default.BrainXmlName);

            var mainElement = doc.Descendants().FirstOrDefault();

            var element = new XElement("Grammar");

            element.SetAttributeValue("Text", textToAddToXml);

            element.SetAttributeValue("Type", command);

            mainElement.Add(element);

            doc.Save(Properties.Settings.Default.BrainXmlLocation + Properties.Settings.Default.BrainXmlName);

            Settings.Default.IsWaitingForResponse = false;

            Settings.Default.IsWaitingForResponseForWord = string.Empty;

            Settings.Default.Save();
        }

        private static XElement InputExistsInXml(string text)
        {
            var document = XDocument.Load(Properties.Settings.Default.BrainXmlLocation + Properties.Settings.Default.BrainXmlName);
           
            try
            {
                var status =  SearchDocument(document.Descendants(), text);

                return status;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static XElement SearchDocument(IEnumerable<XElement> descendants, string text)
        {
            foreach (var descendant in descendants)
            {
                if (descendant.Descendants().Any())
                {
                    SearchDocument(descendants.Descendants(), text);
                }
                else
                {
                    if (descendant.Attribute("Text").Value.ToLower() == text)
                    {
                        return descendant;
                    }
                }
            }
            return null;
        }

        //public static IJarvisEvent GetEvent(string text)
        //{
        //    text = text.ToLower();

        //    if (text.StartsWith("open"))
        //    {
                
        //    }
        //    if (text.StartsWith("jarvis"))
        //    {
        //        if (text.Contains("are") && text.Contains("you") && text.Contains("awake"))
        //        {
        //            return new JarivsInternalEvents.JarvisAwakeCommand();
        //        }
        //        if (text.Contains("goto sleep"))
        //        {
        //            return new JarivsInternalEvents.JavisSleepCommand();
        //        }
        //    }

        //    return null;
        //}

        
        
    }
}
