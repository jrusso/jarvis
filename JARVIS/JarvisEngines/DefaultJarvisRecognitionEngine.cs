﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Windows.Forms;
using System.Xml.Linq;
using JARVIS.Extensions;
using JARVIS.Services;
using JARVISV2;

namespace JARVIS.JarvisEngines
{
    public class DefaultJarvisRecognitionEngine : SpeechRecognitionEngine
    {
        private SpeechSynthesizer mouth;

        public DefaultJarvisRecognitionEngine(SpeechSynthesizer mouth)
        {
            this.mouth = mouth;

            SpeechRecognized += OnSpeechRecognized;

            SpeechRecognitionRejected += OnSpeechRecognitionRejected;

            LoadGrammarCompleted += OnLoadGrammarCompleted;

            LoadGrammarAsync(InitGrammarSet());

            SetInputToDefaultAudioDevice();

            RecognizeAsync(RecognizeMode.Multiple);
        }

        private void OnLoadGrammarCompleted(object sender, LoadGrammarCompletedEventArgs loadGrammarCompletedEventArgs)
        {
         //   mouth.SpeakAsync("I've loaded the default set sir. ");
        }

        private void OnSpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs speechRecognitionRejectedEventArgs)
        {
            if (!Form1.IsSleeping)
            {
                var newTerm = JarvisEventFactory.CheckAgainstGoogle(speechRecognitionRejectedEventArgs);

                if (!string.IsNullOrEmpty(newTerm))
                {
                    var options = WSSQL.GetApplication(newTerm);
                }
            }
        }

        private void OnSpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            var text = e.Result.Text;

            if (!Form1.IsSleeping)
            {
                GetJarvisAction(text);
            }
            else
            {
                if (text.Clean() == "Jarvis are you there?".Clean())
                {
                    Form1.IsSleeping = false;

                    mouth.Speak("For you sir always");
                }
            }
        }

        private void GetJarvisAction(string text)
        {
            if (text.Contains("Jarvis "))
            {
                text = text.Replace("Jarvis ", "");
            }
            var doc = XDocument.Load(@"../../GrammarSets.xml");

            var set = doc.Descendants("GrammarSets");

            XElement desiredElement = null;

            foreach (var xElement in set)
            {
                desiredElement = xElement.Descendants().FirstOrDefault(a => a.Attribute("Text").Value.Clean() == text.Clean());

                if (desiredElement != null)
                {
                    break;
                }
            }

            if (desiredElement != null)
            {
                if (desiredElement.Attribute("Type").Value == "Word")
                {
                    if (desiredElement.Attribute("Response") != null)
                    {
                        mouth.SpeakAsync(desiredElement.Attribute("Response").Value);
                    }
                    else
                    {
                        mouth.SpeakAsync("I will eventually add a response for you.");
                    }
                }
                else if (desiredElement.Attribute("Type").Value == "Command")
                {
                    if (desiredElement.Attribute("Internal") != null && !string.IsNullOrEmpty(desiredElement.Attribute("Internal").Value))
                    {
                        var method = this.GetType().GetMethod(desiredElement.Attribute("Action").Value);

                        if (method != null)
                        {
                            if (method.GetParameters().Any())
                            {
                                var objectParams = new object() {};

                                foreach (var parameter in method.GetParameters())
                                {
                                   
                                }

                                method.Invoke(this, new[] {desiredElement.Attribute("Response") == null ? "" : desiredElement.Attribute("Response").Value});
                            }

                           
                        }
                    }
                    else
                    {
                        if (desiredElement.Attribute("Action") != null)
                        {
                            mouth.SpeakAsync("Let me start that for you sir");

                            Process.Start(desiredElement.Attribute("Action").Value);
                        }
                        else
                        {
                            mouth.Speak("I'm not to sure what to do with that right now.");

                        }

                        if (desiredElement.Attribute("Response") != null)
                        {
                            mouth.SpeakAsync(desiredElement.Attribute("Response").Value);
                        }
                    }
                }
            }
        }

        public void RefreshGrammar(string response)
        {
            UnloadAllGrammars();

            LoadGrammarAsync(InitGrammarSet());

            mouth.Speak(response);

        }

        public void QuitListening(string response)
        {
            Form1.IsSleeping = true;

            mouth.Speak(response);
        }

        Grammar InitGrammarSet()
        {
            var choices = new Choices();

            var document = new XDocument();

            document = XDocument.Load(@"../../GrammarSets.xml");

            var set = document.Descendants("GrammarSets");

            foreach (var nodeSet in set)
            {
                var children = nodeSet.Descendants();

                foreach (var child in children)
                {
                    if (child.Attribute("Type").Value == "Command")
                    {
                        choices.Add("Jarvis " + child.Attribute("Text").Value.Replace('"', ' '));
                    }
                    else
                    {
                        choices.Add(child.Attribute("Text").Value.Replace('"', ' '));
                    }
                }
            }

            return new Grammar(new GrammarBuilder(choices));
        }

    }
}
